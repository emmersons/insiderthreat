package insiderthreat;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

public class DumbHasDateTime implements HasDateTime {
	private DateTime dateTime;
	
	public DumbHasDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	public DateTime getDateTime() {
		return dateTime;
	}
}
