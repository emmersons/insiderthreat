package insiderthreat;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;

public class BetweenDatesFilterTest {

	@Test
	public void testIfDateIsOutOfRangeFilterShouldReturnFalse() {
		BetweenDatesFilter filter = new BetweenDatesFilter(
			new DateTime(2017, 6, 1, 12, 0, 0),
			new DateTime(2017, 6, 15, 12, 0, 0)
		);
		
		DumbHasDateTime ddt = new DumbHasDateTime(new DateTime(2017, 6, 18, 12, 0, 0));
		assertFalse(filter.isWithinPeriod(ddt));
	}

	@Test
	public void testIfDateIsWithinRangeFilterShouldReturnTrue() {
		BetweenDatesFilter filter = new BetweenDatesFilter(
			new DateTime(2017, 6, 1, 12, 0, 0),
			new DateTime(2017, 6, 15, 12, 0, 0)
		);

		DumbHasDateTime ddt = new DumbHasDateTime(new DateTime(2017, 6, 10, 12, 0, 0));
		assertTrue(filter.isWithinPeriod(ddt));
	}

}
