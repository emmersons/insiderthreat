package insiderthreat;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

public class ForTodayFilterTest {
	
	private ForTodayFilter filter;
	
	@Before
	public void setUp() throws Exception {
		this.filter = new ForTodayFilter();
	}

	@Test
	public void testWhenItsTheCurrentDateFotTodayFilterShouldReturnTrue() {
		assertTrue(
			filter.isWithinPeriod(
				new DumbHasDateTime(DateTime.now())
			)
		);
	}

	@Test
	public void testWhenItsNotTheCurrentDateFotTodayFilterShouldReturnFalse() {
		assertFalse(
			filter.isWithinPeriod(
				new DumbHasDateTime(new DateTime(1990, 1, 1, 0, 0, 0))
			)
		);
	}

}
