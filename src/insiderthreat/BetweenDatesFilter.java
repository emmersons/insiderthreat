package insiderthreat;

import org.joda.time.DateTime;
import org.joda.time.Interval;

public class BetweenDatesFilter implements PeriodFilter {
	private DateTime startDate;
	private DateTime endDate;
	
	public BetweenDatesFilter(DateTime startDate, DateTime endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public boolean isWithinPeriod(HasDateTime item) {
		return new Interval(this.startDate, this.endDate).contains(item.getDateTime());
	}
}
