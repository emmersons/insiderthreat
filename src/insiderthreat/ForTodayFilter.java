package insiderthreat;

import org.joda.time.LocalDate;

public class ForTodayFilter implements PeriodFilter {
	public boolean isWithinPeriod(HasDateTime item) {
		return LocalDate.now().compareTo(new LocalDate(item.getDateTime())) == 0;
	}
}
