package insiderthreat;

public class Device {
	private String id;
	private String name;
		
	public Device(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}
}
