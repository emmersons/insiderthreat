package insiderthreat;

public class User {
	private String id;
	private String name;
	private String email;
	private String domain;
	private String role;

	public User(String id, String name, String email, String domain, String role) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.domain = domain;
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getDomain() {
		return domain;
	}

	public String getRole() {
		return role;
	}
}
