package insiderthreat.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import insiderthreat.activity.Http;

public class HttpReader implements ActivityReader {
	private List<Http> activities;
	
	public HttpReader() {
		this.activities = new ArrayList<Http>();
	}
	
	public void read(String filename) throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		try {
		    String line;

		    while ((line = br.readLine()) != null) {
		    	String[] columns = line.split(",");
		    	
		    	String id = columns[0];
		    	String userId = columns[2].replace("DTAA/", "");
		    	String deviceId = columns[3];
		    	DateTime dateTime = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss").parseDateTime(columns[1]);
		    	String[] attributes = {columns[4]};

		    	activities.add(new Http(id, userId, deviceId, dateTime, attributes));
		    }
		} finally {
		    br.close();
		}
	}
	
	public List<Http> getActivities() {
		return this.activities;
	}
}
