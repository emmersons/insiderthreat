package insiderthreat.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import insiderthreat.activity.Logon;

public class LogonReader implements ActivityReader {
	private List<Logon> activities;
	
	public LogonReader() {
		this.activities = new ArrayList<Logon>();
	}
	
	public void read(String filename) throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		try {
			String line = br.readLine(); // ignore fist line

		    while ((line = br.readLine()) != null) {
		    	String[] columns = line.split(",");
		    	
		    	String id = columns[0];
		    	String userId = columns[2].replace("DTAA/", "");
		    	String deviceId = columns[3];
		    	DateTime dateTime = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss").parseDateTime(columns[1]);
		    	String[] attributes = {columns[4]};

		    	activities.add(new Logon(id, userId, deviceId, dateTime, attributes));
		    }
		} finally {
		    br.close();
		}
	}
	
	public List<Logon> getActivities() {
		return this.activities;
	}
}
