package insiderthreat.io;

import java.util.List;
import java.io.FileNotFoundException;
import java.io.IOException;

import insiderthreat.activity.Activity;

public interface ActivityReader {
	public void read(String filename) throws FileNotFoundException, IOException;
	public List<? extends Activity> getActivities();
}
