package insiderthreat;

public interface PeriodFilter {
	public boolean isWithinPeriod(HasDateTime item);
}
