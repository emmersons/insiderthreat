package insiderthreat;

import org.joda.time.DateTime;

public interface HasDateTime {
	public DateTime getDateTime();
}
