package insiderthreat.activity;

import org.joda.time.DateTime;

public abstract class BaseActivity implements Activity {

	protected String id;
	protected String userId;
	protected String deviceId;
	protected DateTime dateTime;
	protected String[] attributes;
	
	public BaseActivity(String id, String userId, String deviceId, DateTime dateTime, String[] attributes) {
		this.id = id;
		this.userId = userId;
		this.deviceId = deviceId;
		this.attributes = attributes;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getUserId() {
		return this.userId;
	}
	
	public String getDeviceId() {
		return this.deviceId;
	}
	
	public DateTime getDateTime() {
		return this.dateTime;
	}
	
	public String[] getAttributes() {
		return this.attributes;
	}
	
}
