package insiderthreat.activity;

import insiderthreat.HasDateTime;

public interface Activity extends HasDateTime {
	public String getId();
	public String getUserId();
	public String getDeviceId();
	public String[] getAttributes();
}
