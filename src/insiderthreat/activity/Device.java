package insiderthreat.activity;

import org.joda.time.DateTime;

public class Device extends BaseActivity {
	public Device(String id, String userId, String deviceId, DateTime dateTime, String[] attributes) {
		super(id, userId, deviceId, dateTime, attributes);
	}
}
